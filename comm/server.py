'''
Contains server classes.

Created on Apr 13, 2014

@author: dimitrijer
'''

import sys
import socket
import errno
from threading import Thread
from sensei_util.log.Logger import Logger
from sensei_util.comm.ProtocolParser import SimpleProtocolParser
from sensei_util.comm.Handler import HandlerInvoker


class ReaderThread(Thread, Logger):

    '''Thread that communicates with specific client.

    Spawned by main server thread.
    '''

    readerCount = 0
    _DEFAULT_INPUT_BUFFER_SIZE = 4096
    '''Default size of input buffer in bytes.'''

    @staticmethod
    def increment_and_get_counter():
        '''Increments and gets sensor reader counter (used in thread name).

        This should be threadsafe, but we don't create that many threads at
        the same time.
        '''
        ReaderThread.readerCount += 1
        return ReaderThread.readerCount

    def __init__(self, server, sock,
                 input_buffer_size=_DEFAULT_INPUT_BUFFER_SIZE):
        Thread.__init__(self, None, None, 'Reader-%d' % +
                        ReaderThread.increment_and_get_counter())
        Logger.__init__(self)

        # Save client socket and server
        self.sock = sock
        self.server = server
        self.ibuffer = memoryview(bytearray(input_buffer_size))
        self.is_running = True
        self.is_stopped = False

        # spawn a new parser for this thread
        self.parser = self.server.parser()

    def stop(self):

        if not self.is_stopped:

            self.is_stopped = True
            self.is_running = False

            # shutdown the socket, thus preventing further reads/writes
            self.sock.shutdown(socket.SHUT_RDWR)

            # close socket
            self.sock.close()

            # tell server our work is done
            self.server.finished(self)

    def run(self):
        while self.is_running:
            try:
                recv_bytes = self.sock.recv_into(self.ibuffer)
                if recv_bytes == 0:
                    self.debug("Connection closed.")
                    break

                self.trace("Received %d bytes.", recv_bytes)

                # append data
                self.parser.append_packet(self.ibuffer[:recv_bytes])

                # read as many messages as we can
                msg = self.parser.read_single_message()
                while msg is not None:

                    # handle the message
                    self.server.invoker.invoke(msg)

                    # read another
                    msg = self.parser.read_single_message()
            except Exception:
                # unhandled exception, log a bit
                self.error(
                    "Unhandled exception while reading from socket!",
                    exc_info=1)

        # We're done here
        self.stop()


class ServerThread(Thread, Logger):

    '''
    Main server thread that listens for incoming connections and spawns new
    reader threads for each client that connects.
    '''

    def __init__(self, server):
        Thread.__init__(self, None, None, self.__class__.__name__)
        Logger.__init__(self)
        self.server = server
        self.reader_threads = []
        self.is_running = True

    def reader_finished(self, finished_reader):
        self.reader_threads.remove(finished_reader)

    def stop(self):

        if self.is_running:
            self.is_running = False

            # stop all readers first
            for reader in self.reader_threads:
                reader.stop()
                # log a bit
                self.debug(reader.getName() + " stopped.")

            # close the socket
            self.server.socket.shutdown(socket.SHUT_RDWR)
            self.server.socket.close()

            self.debug("Stopped main server thread.")

    def run(self):
        while self.is_running:
            try:
                # accept connections from outside
                (clientsocket, address) = self.server.socket.accept()

                # spawn a new reader thread
                reader = ReaderThread(self.server, clientsocket)
                reader.start()

                # add it to the list
                self.reader_threads.append(reader)

                self.debug("Accepted client from " + str(address))
            except socket.error as e:
                # Invalid argument exception will be raised on shutdown()
                if e.errno != errno.EINVAL:
                    self.error(sys.exc_info())
                    break

        # Stop all readers
        self.stop()


class Server(Logger):

    '''
    Server that runs in the background, accepts new client connections,
    and spawns reader threads to handle them.
    '''

    def __init__(self, port, handlers, default_handler=None,
                 parser=SimpleProtocolParser):
        '''
        Constructor
        '''
        Logger.__init__(self)

        self.is_started = False
        self.port = port
        self.invoker = HandlerInvoker(handlers, default_handler)
        self.parser = parser

    def stop(self):
        '''
        Stops the server.
        '''
        if self.is_started:

            self.is_started = False

            # stop main thread
            self.main_thread.stop()

    def finished(self, reader):
        self.main_thread.reader_finished(reader)

    def start(self):
        '''
        Starts the server.

        Opens up a socket and listens on it for incoming connections.
        '''

        if self.is_started:
            self.error('Server already started!')
            return

        # open up a socket
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(('0.0.0.0', self.port))

        # 5 backlog connections should be enough
        self.socket.listen(5)

        # start up main server thread
        self.main_thread = ServerThread(self)
        self.main_thread.start()

        # log a bit
        self.debug("Listening for incoming connections on %d" % self.port)

        self.is_started = True
