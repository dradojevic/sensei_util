'''
Contains definitions for Handler classes.

Handlers are objects that handle messages with specific message
codes.

Created on May 18, 2014

@author: dimitrijer
'''

import time
import re
from sensei_util.log.Logger import Logger
from sensei_util.comm.Message import MessageWithHandlerCode


class Handler(Logger):

    '''
    Extend this class to handle specific codes.
    '''

    def handle(self, payload):
        raise NotImplementedError

    def __str__(self):
        # ALL_CAPS string representation
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', self.__class__.__name__)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).upper()


class HandlerInvoker(Logger):

    '''
    This class instantiates and invokes _handlers for different
    message codes.

    invoke() method is called by reader threads, after the incoming message has
    been decoded.
    '''

    def __init__(self, handlers, default_handler=None):
        '''
        Instantiates a new Handler.

        -- handlers - a dictionary that maps handler code to specific handler
        '''

        Logger.__init__(self)

        # save handler callbacks
        self._handlers = handlers
        self._default_handler = default_handler

    def _create_handler(self, msg):
        '''
        Creates a new instance of handler to handle msg.
        '''

        # extract handler code
        if isinstance(msg, MessageWithHandlerCode):

            if msg.hc not in self._handlers:
                self.error('Unknown handler code: %d!', msg.hc)
                return None

            # instantiate handler and return it
            return self._handlers[msg.hc]()
        else:
            # message has no handler code, invoke default handler
            return self._default_handler()

    def invoke(self, msg):
        '''
        Handles received message by invoking appropriate handler.
        '''
        try:

            # instantiate handler
            h = self._create_handler(msg)
            if h is None:
                self.error('Failed to find handler to invoke for message!')
                return

            self.debug("Starting %s", str(h))

            # note the time
            starting_time = time.time()

            # invoke handler
            h.handle(msg.payload)

            elapsed_time = time.time() - starting_time

            self.debug("Finished %s in %dms.", str(h), elapsed_time * 1000)

        except Exception:
            hc = msg.hc if isinstance(msg, MessageWithHandlerCode) else None
            self.error('Unhandled exception while handling message [HC=%s]!',
                       str(hc), exc_info=1)
