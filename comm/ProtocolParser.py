'''
Contains definitions of several wire protocols.

Created on Apr 13, 2014

@author: dimitrijer
'''

from bitstring import BitStream, Bits, pack
from sensei_util.comm.Message import MessageWithHandlerCode, Message
from sensei_util.log.Logger import Logger


class ProtocolParser(Logger):

    '''
    Aggregates bytes from socket and assembles individual messages.

    Also encodes messages to appropriate network format. Not threadsafe.
    NB: this is an abstract class which should be extended by concrete
    implementations.
    '''

    def __init__(self, header_length=0):
        '''
        Constructor.

        -- header_length - length of header in bits.
        '''
        Logger.__init__(self)

        self._input_buffer = None
        '''Input buffer to store incoming bytes in.'''

        self._header_length = header_length
        '''Length of message header in bits, if any.'''

    def _read_message_length(self):
        '''
        This method should read message length in bytes from input buffer and
        return it.

        Input buffer should not be reset/rewinded/cleared once message length
        is read.
        '''
        raise NotImplementedError()

    def _read_message(self, payload_length):
        '''
        This method should read message of payload_length bytes from input
        buffer and return it.

        Input buffer should not be reset/rewinded/cleared once message is read.

        -- payload_length - length of payload to read, in bytes
        '''
        raise NotImplementedError()

    def append_packet(self, raw_bytes):
        '''
        Appends incoming raw bytes from socket to input buffer.
        '''
        if self._input_buffer is None:
            self._input_buffer = BitStream(bytes=raw_bytes)
        else:
            self._input_buffer.append(Bits(bytes=raw_bytes))

        self.trace('Appended packet of %d bytes, input buffer length: '
                   '%d bytes',
                   len(raw_bytes), self._input_buffer.len / 8)

    def read_single_message(self):

        # check if we have enough data
        if self._input_buffer.len > self._header_length:

            # reset position
            self._input_buffer.pos = 0

            # get message length
            payload_len = self._read_message_length()

            self.trace('Peeking for a message of %d bits '
                       '(input buffer size: %d bits).',
                       self._header_length + payload_len * 8,
                       self._input_buffer.len)

            # check if we have that much data available
            if self._input_buffer.len >= self._header_length + payload_len * 8:

                # read next message
                msg = self._read_message(payload_len)

                # truncate what we've read
                del self._input_buffer[0:self._input_buffer.pos]

                self.trace('Read %d bytes from input buffer, %d bytes left',
                           self._header_length / 8 + payload_len,
                           self._input_buffer.len / 8)

                # return the new message
                return msg
            else:
                # we needz moar data, reset position
                self._input_buffer.pos = 0

        return None

    def encode_message(self, msg):
        '''
        Encodes message to appropriate format and returns raw bytes.
        '''
        raise NotImplementedError()


class TrivialProtocolParser(ProtocolParser):

    '''
    ProtocolParser for messages without header, and fixed message length
    (no handler code, just payload).

    Messages are in form of:
    |payload|
    where:
    - payload is an unsigned byte (1B) integer, 0 - 255.
    '''

    _PAYLOAD_LEN = 1
    '''Message length in bytes.'''

    def __init__(self):
        super(TrivialProtocolParser, self).__init__()

    def _read_message(self, payload_length):
        payload = self._input_buffer.read(payload_length * 8).bytes
        return Message(payload)

    def _read_message_length(self):
        # message length is constant
        return TrivialProtocolParser._PAYLOAD_LEN

    def encode_message(self, msg):
        return pack('bytes', msg.payload).bytes


class SimpleProtocolParser(ProtocolParser):

    '''
    ProtocolParser for simple messages containing handler code and payload.
    Messages are in form of:
    |ML|HC|payload|
    where:
    - ML is message length (4 bytes)
    - HC is handler code (2 bytes)
    - payload is the actual message body
    '''

    _MESSAGE_LENGTH_FIELD_LEN = 32
    '''Length of payload length field in message header in bits.'''

    _HANDLER_CODE_FIELD_LEN = 16
    '''Length of handler code field in message header in bits.'''

    def __init__(self):
        # header length is message len + hc
        super(SimpleProtocolParser, self).__init__(
            SimpleProtocolParser._MESSAGE_LENGTH_FIELD_LEN +
            SimpleProtocolParser._HANDLER_CODE_FIELD_LEN)

    def _read_message_length(self):
        return self._input_buffer.read(
            SimpleProtocolParser._MESSAGE_LENGTH_FIELD_LEN).uint

    def _read_message(self, payload_length):
        # extract HC and payload
        hc = self._input_buffer.read(
            SimpleProtocolParser._HANDLER_CODE_FIELD_LEN).uint
        payload = self._input_buffer.read(payload_length * 8).bytes
        return MessageWithHandlerCode(hc, payload)

    def encode_message(self, msg):
        return pack('uint:32, uint:16, bytes', len(
            msg.payload), msg.hc, msg.payload).bytes
