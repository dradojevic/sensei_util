'''
Contains base message class definitions.

Messages are simple container classes.

Created on May 18, 2014

@author: dimitrijer
'''


class Message(object):

    '''
    Message represents a single message received by server thread.

    It contains payload.
    '''

    def __init__(self, payload):
        self.payload = payload


class MessageWithHandlerCode(Message):

    '''
    Message along with a handler code.
    '''

    def __init__(self, hc, payload):
        '''
        Constructor
        '''
        super(MessageWithHandlerCode, self).__init__(payload)

        self.hc = hc
