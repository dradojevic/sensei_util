'''
Created on Apr 13, 2014

@author: dimitrijer
'''

import os
import logging
from logging.handlers import TimedRotatingFileHandler
import sys

TRACE = 5
'''Trace logging level. Has the lowest value of all logging levels.'''


class Logger(object):

    '''
    Logger implements logging functionalities.

    Always call init_logging() prior to logging anything.
    '''

    @staticmethod
    def init_logging(loggers=[logging.getLogger()], log_trace=False):
        '''
        Initialize logging stuff and handlers.
        '''

        # Create root directory, if it does not exist
        if not os.path.exists('log'):
            os.makedirs('log')

        # Add TRACE name
        logging.addLevelName(TRACE, 'TRACE')

        # Set up format of root messages
        formatter = logging.Formatter(
            "%(asctime)s [%(threadName)s] %(levelname)s %(name)s - %(message)s"
        )

        # Store handlers here
        handlers = []

        # Add three handlers: console (stdout), rotating debug or trace
        # file and rotating error file
        consoleHandler = logging.StreamHandler(stream=sys.stdout)
        if log_trace:
            consoleHandler.setLevel(TRACE)
        else:
            consoleHandler.setLevel(logging.DEBUG)
        consoleHandler.setFormatter(formatter)
        handlers.append(consoleHandler)

        if log_trace:
            traceFileHandler = TimedRotatingFileHandler(
                'log/trace.log',
                when='midnight')
            traceFileHandler.setLevel(TRACE)
            traceFileHandler.setFormatter(formatter)
            handlers.append(traceFileHandler)
        else:
            debugFileHandler = TimedRotatingFileHandler(
                'log/debug.log',
                when='midnight')
            debugFileHandler.setLevel(logging.DEBUG)
            debugFileHandler.setFormatter(formatter)
            handlers.append(debugFileHandler)

        errorFileHandler = TimedRotatingFileHandler(
            'log/error.log',
            when='midnight')
        errorFileHandler.setLevel(logging.ERROR)
        errorFileHandler.setFormatter(formatter)
        handlers.append(errorFileHandler)

        for logger in loggers:
            # clear any previous handlers and add new ones
            logger.handlers = []

            for h in handlers:
                logger.addHandler(h)

            if log_trace:
                logger.setLevel(TRACE)
            else:
                logger.setLevel(logging.DEBUG)

    def __init__(self, name=None):
        '''
        Constructor
        '''
        self.log = logging.getLogger(
            self.__class__.__name__ if name is None else name)
        self.log.setLevel(TRACE)

    def trace(self, msg, *args, **kwargs):
        '''
        Log message of trace level.
        '''
        self.log.log(TRACE, msg, *args, **kwargs)

    def debug(self, msg, *args, **kwargs):
        self.log.debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.log.info(msg, *args, **kwargs)

    def warn(self, msg, *args, **kwargs):
        self.log.warn(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.log.error(msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        self.log.critical(msg, *args, **kwargs)
